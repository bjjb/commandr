# Run me with `observr tests.watchr`

watch('^test/.*_test\.rb') { |m| ruby m[0] }
watch('^lib/(.*)\.rb') { |m| ruby "test/#{m[1]}_test.rb" }
watch('^test/test_helper\.rb') { |m| ruby tests }

Signal.trap('QUIT') { ruby tests } # CTRL-\
Signal.trap('INT') { abort("\n") } # CTRL-C

def ruby(*paths)
  system "ruby -Itest:lib #{paths.join(" ")}"
end

def tests
  Dir["test/**/*_test.rb"]
end
