# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'commandr/version'

Gem::Specification.new do |spec|
  spec.name          = "commandr"
  spec.version       = Commandr::VERSION
  spec.authors       = ["Bryan JJ Buckley"]
  spec.email         = ["jjbuckley@gmail.com"]
  spec.description   = %q{Library for quickly writing command-line-interfaces in Ruby}
  spec.summary       = %q{Ruby CLI library}
  spec.homepage      = "http://github.com/bjjb/commandr"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "minitest"
end
