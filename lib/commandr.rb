class Commandr
  # The option parser that will be used for this command
  def options
    self.class.options
  end

  # Called to run the command (by #start, after parsing the options)
  def run(command, argv)
    commands.fetch(command).new.start(argv)
  end

  # Subcommands (a hash of name => command class)
  def commands
    self.class.commands
  end

  # Start processing argv
  def start(argv)
    catch(:done) do
      options.parse!(argv)
      puts options if argv.empty?
      run(argv.shift, argv) until argv.empty?
    end
  end

  # A summary of the usage.
  def summary
    @summary ||= [option_summary, command_summary].compact.join("\n\n")
  end

  def program_summary
    @program_name ||= self.class.program_summary
  end

  def option_summary
    @options.to_s if options?
  end

  def command_summary
    max_width = commands.keys.sort(&:length).last.length
    commands.map do |name, command|
      "    %#{max_width}s    %s" % [name, command.description]
    end.join("\n")
  end

  def options?
    self.class.options?
  end

  def commands?
    self.class.commands?
  end

  def program_name
    @program_name ||= self.class.program_name
  end

  class << self
    def options?
      !options.empty?
    end

    def commands?
      !commands.empty?
    end

    def program_name
      @program_name ||= "unnamed"
    end

    def program_name=(program_name)
      @program_name = program_name
    end

    def program_summary
      @program_summary ||= [ "Usage:\n    #{program_name}" ].tap do |a|
        a << "[options]" if options?
        a << "[command [args]]" if commands?
      end.join(' ')
    end

    def options(*args)
      OptionParser.new do |o|
        o.banner = program_summary + "\n\nOptions:"
        @options.each do |option|
          o.on(*option)
        end
      end
    end

    def commands
      @commands ||= {}
    end

    def option(*args)
      @options ||= []
      @options << args
    end

    def command(name, &block)
      commands[name] = Class.new(self, &block).tap { |c| c.name = name }
    end

    def start(argv = ARGV)
      new.start(argv)
    end

    def description
      @description ||= "No description"
    end

    def description=(desc)
      @description = desc
    end

    def desc(desc)
      self.description = "No description"
    end

    def command_name
      @command_name ||= program_name
    end

    def name_width
      commands.map(&:command_name).map(&:length).sort.last
    end

    def name
      @name ||= to_s
    end

    def name=(name)
      @name = name
    end
  end
end
