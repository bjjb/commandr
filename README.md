# Commandr

A library for writing command-line tools

## Installation

Add this line to your application's Gemfile:

    gem 'commandr'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install commandr

## Usage

Write a class that includes `Commandr`, set up options, and call it with
`MyClass.start!`.

## Examples

### A rubygems-like tool

```ruby
#!/usr/bin/env ruby
require 'commandr'
class MyRubygems
  include Commandr
  option '-h', '--help', 'Print help'
  option '-v', '--version', :version, 'Print the version number'
  commands :build, :cert, :cleanup
end

MyRubygems.start!
``` 

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
