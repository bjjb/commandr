require 'test_helper'
require 'commandr'

describe Commandr do
  it "works" do
    out, err = capture_io do
      program.start(["-h"])
    end
    out.must_equal program.summary
  end

  let :program do
    Class.new(Commandr) do
      self.program_name = "dummy"

      option("-h", "--help", "Print this", :NONE) do
        puts usage
      end

      option("-V", "--version", "Print version", :NONE) do
        puts "v0.0.1"
      end

      option("-v", "--verbose", "Be noisy", :OPTIONAL) do |verbose|
        @verbose = !!verbose
      end

      command(:help) do
        def run(argv)
          puts "RUNNING HELP"
        end
      end
    end
  end
end
